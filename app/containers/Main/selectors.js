// ------ sending a 'slice' parameter to a makeSelector function is a way to get it to accept a more
// specific chunk of the reducer: useful for calling the selector from the reducer.

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  dot, dot1, whenOk, ifOk, gt, ok, ne, eq, nieuw1, laats,
  exception, raise, ifFalse, ifTrue,
} from 'stick-js'

import { createSelector, defaultMemoize as memoize, } from 'reselect'

import {
  length, isNotEmptyObject, notOk,
} from '../../utils/utils.js'

import {
  createLengthEqualSelector,
} from '../../utils/utils-select'

import {
  find as iFind,
  filter as iFilter,
  sortBy as iSortBy,
  get, getIn, size, toJS, fromJS,
  valueSeq,
} from '../../utils/utils-immutable'

// const selectDomain = get ('SLICE') // domain, api, container name ...
//
// export const makeSelectError = _ => createSelector (
//   selectDomain,
//   get ('error'),
// )
//
// export const makeSelectIsAlteredHasDelete = _ => createSelector (
//   selectDomain,
//   getIn (['modifications', 'numDelete']) >> ne (0),
// )
//
