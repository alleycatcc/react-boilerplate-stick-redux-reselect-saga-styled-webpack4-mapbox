defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  join, tap, not, sprintfN, ifOk, whenOk, assocM, noop,
  list, defaultTo,
  lets,
  prop,
} from 'stick-js'

import React, { PureComponent, Component, } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { FormattedMessage, } from 'react-intl'
import { createStructuredSelector, } from 'reselect'

import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'
import makeSelectMain from './selectors'
import reducer from './reducer'
import saga from './saga'
import messages from './messages'

import Swiper from 'react-id-swiper'
import Switch from 'react-toggle-switch'
import 'react-toggle-switch/dist/css/switch.min.css'

import {
  mapX, tellIf,
  iwarn, ierror,
  logAndroidPerf,
  logWith,
} from '../../utils/utils'

import {
  toJS,
} from '../../utils/utils-immutable'

import {
  whyYouRerender,
} from '../../utils/utils-react'

import {
  error as alertError,
} from '../../utils/utils-alert'

import {
  configGetOrDie, configGet,
  configGetComponent, configGetsComponent,
} from '../../config'

import {
  changeLocation,
  updateDebug,
  updateShowRain,
} from '../App/actions'

import {
  makeSelectLocation,
  makeSelectForecastByTime,
  makeSelectClockSegments,
} from '../../domains/domain/selectors'

import {
  makeSelectDebug,
} from '../../domains/app/selectors'

import Clock from '../../components/Clock'

import {
  spinner,
} from '../../components/General'

const locationName = loc => ['locationData', loc, 'name']
  | configGetOrDie

const expandDateTime = str => lets (
  _ => new Date (str),
  d => d.getHours (),
  d => d.getMinutes (),
  (_, h, m) => [h, m] | sprintfN ('%02d:%02d')
)

// --- xxx update
// const touchRatio = 'touchRatio' | configGetComponent ('Main')
const touchRatio = 1

const LocationHeaderS = styled.div`
  margin-top: 5%;
  margin-bottom: 2%;
  font-size: 30px;
  color: white;
  text-align: center;
  white-space: nowrap;

  div {
    border-bottom: 1px solid #dc7b7b;
    display: inline-block;
  }
`

const SwitchS = styled.div`
  ${ 'leftRight' | prop };

  font-size: 15px;
  position: absolute;
  bottom: 1%;
  width: 90px;
  display: inline-block;
  div {
    vertical-align: top;
    display: inline-block;
    margin-right: 3%;
  }
  .a-switch {
    .switch-toggle {
    }
  }
`

const ASwitch = (text, leftRight) => ({ active, onClick, }) =>
  <SwitchS leftRight={leftRight}>
    <div>{text}</div>
    <Switch
      onClick={onClick}
      className={'a-switch'}
      on={active}
    />
  </SwitchS>

const DebugSwitch = ASwitch ('mock data', 'left: 40px')
const RainSwitch  = ASwitch ('drip drip', 'right: 40px')

const LocationHeader = ({ location, }) => <LocationHeaderS>
  <div>
    {location}
  </div>
</LocationHeaderS>

const vertel = list >> join (',') >> tap (console.log) >> alertError

const getNewLocOld = (op, { routesReverse, routes, location, }) => {
  const curIdx = routesReverse [location] | Number
  const newIdx = curIdx | op | String
  return routes [newIdx]
}

const getNewLoc = (routes, idx) => routes [idx]
  | defaultTo (_ => ierror ('invalid tab', idx))

const xxxableSwiper = assocM ('allowTouchMove')
const enableSwiper  = xxxableSwiper (true)
const disableSwiper = xxxableSwiper (false)

const forecastSection = (forecastByTime, clockSegments, swiperInstance) => clockSegments | ifOk (
  (segments) => <Clock
    onClickStart={_ => disableSwiper (swiperInstance)}
    onClickEnd={_ => enableSwiper (swiperInstance)}
    segments={clockSegments}
    forecastByTime={forecastByTime}
  />,

  _ => spinner (),
)

const tab = (curIdx, forecastByTime, clockSegments, swiperInstance) => (loc, idx) => <div key={idx}>
  <LocationHeader
    location={loc | locationName}
  />
  {(idx == curIdx) && forecastSection (forecastByTime, clockSegments, swiperInstance)}
</div>

// --- This component is a class, not because we need state, but because the tabs (might) need to be able to
// access the most current version of this.props on each transition.

export class Main extends PureComponent {
  constructor (props) {
    super (props)
    this.swiperInstance = void 8
  }

  swiperParams () {
    const { props, } = this
    const { changeLocationDispatch, routes, routesReverse, } = props

    return {
      touchRatio,

      on: {
        init: noop,
// passiveListeners: false,
        slideChangeTransitionStart: _ => lets (
          _ => this.swiperInstance | whenOk (
            si => getNewLoc (routes, si.activeIndex)
          ),
            // newLoc => newLoc && changeLocationDispatch (newLoc),
          newLoc => setTimeout (_ => {
            logAndroidPerf ('Main: dispatching CHANGE_LOCATION')
            if (newLoc) changeLocationDispatch (newLoc)
          }, 10)
        ),
      },
    }
  }

  swiper (location='oost', routesReverse) {
    const { forecastByTime, clockSegments, } = this.props
    const curIdx = routesReverse [location]
    if (this.swiperInstance) this.swiperInstance.slideTo (curIdx)

    return <Swiper {... this.swiperParams ()}
      initialSlide={routesReverse [location] | Number}
      ref={whenOk (({ swiper, }) => {
        this.swiperInstance = swiper
        // --- to disable:
        // this.swiperInstance.allowTouchMove = false
      }
      )}
    >
      {['oost', 'zuidoost', 'pijp', 'centrum'] | mapX (tab (curIdx, forecastByTime, clockSegments, this.swiperInstance))}
    </Swiper>
  }

  componentWillReceiveProps (nextProps) {
    whyYouRerender ('Main', this.props, nextProps)
  }

  render () {
	// debug --- ok here or better as state? xxx
    const {
      debug, showRain, updateDebugDispatch, updateShowRainDispatch,
      changeLocationDispatch, location, routes, routesReverse,
    } = this.props
    logAndroidPerf ('Main rerender')
    return <div>
      {this.swiper (location, routesReverse)}
      <DebugSwitch
        active={debug}
		onClick={() => debug | not | updateDebugDispatch}
      />
      <RainSwitch
        active={showRain}
		onClick={() => showRain | not | updateShowRainDispatch}
      />
    </div>
  }
}

Main.propTypes = {
}

const mapStateToProps = createStructuredSelector ({
  location: makeSelectLocation (),
  forecastByTime: makeSelectForecastByTime (),
  clockSegments: makeSelectClockSegments (),
  debug: makeSelectDebug (),
})

const mapDispatchToProps = (dispatch) => ({
  changeLocationDispatch: changeLocation >> dispatch,
  updateDebugDispatch: updateDebug >> dispatch,
  updateShowRainDispatch: updateShowRain >> dispatch,
  // showAddKeyDispatch: showAddKey >> dispatch,
  // readerIDDispatch: path (['target', 'value']) >> changeReaderID >> dispatch,
})

export default Main
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'main' })
  // --- local reducer (optional)
  | injectReducer ({ reducer, key: 'main' })


