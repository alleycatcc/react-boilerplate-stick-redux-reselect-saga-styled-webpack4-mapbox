// --- this was made into its own container in order to keep the entire homepage from rerendering on
// location change and hopefully make it snappier.

defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  defaultToV,
} from 'stick-js'

import React, { PureComponent, Component, } from 'react'
import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { FormattedMessage, } from 'react-intl'
import { createStructuredSelector, } from 'reselect'

import injectSaga from 'utils/injectSaga'
import injectReducer from 'utils/injectReducer'
import makeSelectNavBar from './selectors'
import reducer from './reducer'
import saga from './saga'
import messages from './messages'

import {
  mapX, tellIf,
  iwarn, ierror,
  logAndroid,
  logAndroidPerf,
} from '../../utils/utils.js'

import {
  toJS,
} from '../../utils/utils-immutable'

import {
  whyYouRerender,
} from '../../utils/utils-react.js'

import {
  error as alertError,
} from '../../utils/utils-alert.js'

import config from '../../config'

// import { } from '../App/actions'
// import { } from '../../domains/api/selectors'
import {
  makeSelectLocation,
} from '../../domains/domain/selectors'
// import { } from '../../domains/ui/selectors'
// import { } from '../../components/xxx'

const NavBarS = styled.div`
  font-size: 20px;
  div {
    display: inline-block;
  }
  width: 280px;
  margin: auto;
`

const NavBarItemS = styled.div`
  width: 50px;
  margin-left: 10px;
  margin-right: 10px;
  padding: 0px;
  height: 70px;
  img {
    filter: brightness(133%);
  }
  &.__unselected {
    img {
      margin: 2px;
    }
  }
  &.__selected {
    img {
      // transform: rotateY(360deg);
      transition: transform .5s;
      border: 2px dashed white;
      border-radius: 10000px;
    }
    // border-bottom: 1px solid white;
  }
`

const NavBarItem = ({ idx, location, curTab, }) =>
  <NavBarItemS className={curTab == idx ? '__selected' : '__unselected'}>
    <a href={logAndroidPerf ('(re)rendering NavBarItem, idx'), '#/' + location}>
      <img src={config.images.navBar[location]} />
    </a>
  </NavBarItemS>

export const NavBar = ({ location, routesReverse, }) => {
    const curTab = routesReverse [location] | defaultToV ('0') | Number

    return <NavBarS>
      <NavBarItem idx={0} curTab={curTab} location='oost'/>
      <NavBarItem idx={1} curTab={curTab} location='zuidoost'/>
      <NavBarItem idx={2} curTab={curTab} location='pijp'/>
      <NavBarItem idx={3} curTab={curTab} location='centrum'/>
    </NavBarS>
}

NavBar.propTypes = {
}

const mapStateToProps = createStructuredSelector ({
  location: makeSelectLocation (),
})

const mapDispatchToProps = (dispatch) => ({
  // showAddKeyDispatch: showAddKey >> dispatch,
  // readerIDDispatch: path (['target', 'value']) >> changeReaderID >> dispatch,
})

export default NavBar
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'navBar' })
  // --- local reducer (optional)
  | injectReducer ({ reducer, key: 'navBar' })


