import React from 'react';
import { FormattedMessage } from 'react-intl';
import styled from 'styled-components';

import messages from './messages';

const H1 = styled.h1`
  font-size: 2em;
`

export default function NotFound(props) {
  return (
    <article>
        404'ed!
    </article>
  );
}
