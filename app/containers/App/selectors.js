defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import { createSelector, } from 'reselect'

import {
  pipe, compose, composeRight,
  dot, dot1, whenOk, ifOk,
} from 'stick-js'

import {
  length,
} from '../../utils/utils'

import {
  get, toJS,
} from '../../utils/utils-immutable'

export const selectGlobal = get ('global')
export const selectRoute = get ('route')

export const makeSelectTestMsg = _ => createSelector (
  selectGlobal,
  get ('testMsg'),
)

// --- used? xxx
export const makeSelectLocation = () => createSelector (
  selectRoute,
  // @todo
  get ('location').toJS (),
)

/** some examples:
*
* export const makeSelectError = _ => createSelector (
*   selectDomain,
*   get ('error'),
* )
*
* export const makeSelectIsAlteredHasDelete = _ => createSelector (
*   selectDomain,
*   getIn (['modifications', 'numDelete']) >> ne (0),
* )
*
* export const makeSelectIsAltered = _ => createSelector (
*   makeSelectIsAlteredHasAdd (),
*   makeSelectIsAlteredHasDelete (),
*   makeSelectAssignedKeysCanonical (),
*   makeSelectAssignedKeysData (),
*   makeSelectAssignedKeysNames (),
*
*   (hasAdd, hasDelete, canonical, data, names) =>
*     (hasAdd || hasDelete) ? true :
*     hasAny ({ canonical, names, data, })
* )
*
* export const makeSelectAssignedKeyIsAdded = _ => createSelector (
*   makeSelectAssignedKeysMeta (),
*   (_, props) => props.code,
*   (meta, code) => meta
*     | getIn ([code, 'add'])
*     | Boolean
* )
*
* // --- uses metadata tag `update`, so it's dumber than makeSelectAssignedKeyIsAltered.
* export const makeSelectAssignedKeyIsAlteredAtAll = _ => createSelector (
*   makeSelectAssignedKeysMeta (),
*   (_, props) => props.code,
*   (meta, code) => meta
*     | getIn ([code, 'update'])
*     | Boolean
* )
*
* const curStateExistingKey = ({ code, names, data, }) => ({
*   code,
*   name: names.get (code),
*   ... data.get (code) | whenOk (toJS),
* })
*/
