defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

const forceStaticMap = process.env.FORCE_STATIC_MAP == '1'

import {
  pipe, compose, composeRight,
  ifTrue, sprintf1,
} from 'stick-js'

import React from 'react'
import { Helmet } from 'react-helmet'
import styled from 'styled-components'
import { Switch, Route } from 'react-router-dom'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import config from '../../config'

import {
  Alert,
} from '../../components/General'

import injectReducer from 'utils/injectReducer'
import injectSaga from 'utils/injectSaga'

import domainReducer from '../../domains/domain/reducer'
import uiReducer from '../../domains/ui/reducer'
import appReducer from '../../domains/app/reducer'
// --- not used xxx
import apiReducer from '../../domains/api/reducer'

import {
  makeSelectError,
} from '../../domains/domain/selectors'

import HomePage from 'containers/HomePage/Loadable'
import NotFoundPage from 'containers/NotFoundPage/Loadable'

import saga from './saga'

const bgs = {
//   '1.33': require ('../../images/fritz-60s-portrait-1.33.png'),
//   '1.60': require ('../../images/fritz-60s-portrait-1.60.png'),
//   '1.67': require ('../../images/fritz-60s-portrait-1.67.png'),
//   '1.77': require ('../../images/fritz-60s-portrait-1.78.png'),
//   '1.78': require ('../../images/fritz-60s-portrait-1.78.png'),
//   default: require ('../../images/fritz-60s-portrait.png'),
}

const localConfig = {
  images: {
      bg: () => {
        const cw = document.body.clientWidth
        const ch = document.body.clientHeight
        const aspRat = (!cw || !ch)
          ? undefined
          : (ch / cw) | sprintf1 ('%.2f')
        return bgs [aspRat] || bgs ['default']
      },
  },
}

const fontLora = require ('../../fonts/lora.woff2')

const AppWrapper = styled.div`
  @font-face {
    src: url("${_ => fontLora}") format("woff2");
    font-family: "Lora";
  }
  font-family: 'Lora', sans-serif;
  position: relative;
  overflow-x: hidden;
  overflow-y: hidden;
  margin: 0 auto;
  display: flex;
  height: 100%;
  // padding: 0 16px;
  flex-direction: column;
`

class App extends React.PureComponent {
  render () {
    const { error, } = this.props
    const { title, } = config

    return error | ifTrue (_ =>
      <div>
        <div>Sorry, but we've encountered a fatal error.</div>
        <div>Please reload the page and start again.</div>
      </div>
    ) (_ =>
      <AppWrapper>
        <Helmet
          titleTemplate={"%s - " + title}
          defaultTitle={title}
        >
          <meta name="description" content="" />

        </Helmet>
        <Switch>
          <Route exact path="/" render={props => <HomePage
              route={props}
              staticMap={forceStaticMap}
            />}
          />
          <Route exact path="/static-map" render={props => <HomePage
              route={props}
              staticMap={true}
            />}
          />
          <Route path="" component={NotFoundPage} />
        </Switch>
        {Alert ()}
      </AppWrapper>
    )
  }
}

const mapDispatchToProps = (dispatch) => ({
})

const mapStateToProps = createStructuredSelector ({
  error: makeSelectError (),
})

export default App
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'app' })
  | injectReducer ({ reducer: domainReducer, key: 'domain' })
  | injectReducer ({ reducer: uiReducer,     key: 'ui' })
  | injectReducer ({ reducer: apiReducer,    key: 'api' })
  | injectReducer ({ reducer: appReducer,    key: 'app' })
