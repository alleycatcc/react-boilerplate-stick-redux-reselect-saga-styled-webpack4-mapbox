defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import { fromJS, } from 'immutable'

import {
  pipe, compose, composeRight,
  ok, mergeFrom, whenOk, dot, xMatch,
  whenPredicate, eq,
  concatFrom, blush, ifOk,
} from 'stick-js'

const initialState = fromJS ({
})

export default function appReducer (state = initialState, action) {
  switch (action.type) {
    default:
      return state
  }
}
