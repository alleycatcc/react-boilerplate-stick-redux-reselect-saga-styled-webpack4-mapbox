defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import { curry, } from 'ramda'
import zip from 'ramda/src/zip'

import {
  pipe, compose, composeRight,
  split, prop, map, join, assoc,
  whenOk, sprintf1,
  eq, not, ifPredicate, whenPredicate,
  ifOk,
  ifTrue,
  prependTo,
  sprintfN,
  bindProp,
  whenTrue,
  concat, divideBy,
  whenFalse,
  always,
  mergeM,
  id,
  tap,
} from 'stick-js'

const { log, } = console
const logWith = (header) => (...args) => log (... [header, ...args])

import {
  iwarn, ierror,
  logAndroidPerf,
  ifTrueV,
  defaultToV,
} from '../../utils/utils.js'

import React, { PureComponent, Component, } from 'react'

import PropTypes from 'prop-types'
import styled from 'styled-components'
import { connect, } from 'react-redux'
import { createStructuredSelector, } from 'reselect'

import mapboxgl from 'mapbox-gl'
mapboxgl.accessToken = 'pk.eyJ1IjoiYWxsZW4tYWxsZXljYXQiLCJhIjoiY2pqYnpuc2tlM2x6MTNrcWd2aXY0ZXZvdCJ9.Uczwxw49A7dd3yQG2kjPEA';

import RCSlider from 'rc-slider'
import 'rc-slider/assets/index.css'

import ReactAudioPlayer from 'react-audio-player'

import { configGetOrDie, configGetsOrDie, } from '../../config'

import { fetchForecast, changeLocation, } from '../App/actions'

import { makeSelectShowRain, } from '../../domains/app/selectors'

import {
  makeSelectLoading as makeSelectApiLoading,
} from '../../domains/api/selectors'

// import { } from '../../domains/domain/selectors'

import {
  makeSelectAddingKey, makeSelectAddingKeysFromCSV,
  makeSelectAnalysingEvents,
} from '../../domains/ui/selectors'

import {
  spinner,
} from '../../components/General'

import injectReducer from 'utils/injectReducer'
import injectSaga from 'utils/injectSaga'
import saga from './saga'

import {
  mapX, tellIf, isNotEmptyList,
} from '../../utils/utils.js'

import {
  toJS,
} from '../../utils/utils-immutable'

import {
  whyYouRerender,
} from '../../utils/utils-react.js'

import {
  error as alertError,
} from '../../utils/utils-alert.js'

// --- simple check for mobile -- not really good though (monitor could have touch events)
const isMobile = _ => 'ontouchstart' in document.documentElement

const localConfig = {
  verbose: true,
  debug: {
    render: false,
  },
}

const tell = tellIf (localConfig.verbose)

const markerRefs = {}

const Article = styled.article`
  height: 100%;
  margin: 0px;
  padding: 0px;
  color: black;
`

const TopS = styled.div`
  position: relative;
  margin-left: auto;
  margin-right: auto;
  margin-top: 0px;
  margin-bottom: 0px;
  padding: 0px;
  font-size: 20px;
  height: 100%;
`

const Top = ({ children, }) => <TopS>{children}</TopS>

const stipjeRadiusMobile = 11
const stipjeRadiusDesktop = 17

const stipjeRadius = {
  mobile: 11,
  desktop: 17,
}

const stipjeScale = x => type => _ => stipjeRadius [type] | divideBy (x) | String | concat ('px')

const stipjeWhole = 1 | stipjeScale
const stipjeHalf  = 2 | stipjeScale

const HotSpotS = styled.div`
  @media only screen and (max-width: 768px) {
    width:  ${ 'mobile' | stipjeWhole };
    height: ${ 'mobile' | stipjeWhole };
    top:  calc(${ prop ('top')  >> String }% - ${ 'mobile' | stipjeHalf });
    left: calc(${ prop ('left') >> String }% - ${ 'mobile' | stipjeHalf });
  }
  @media only screen and (min-width: 768px) {
    width:  ${ 'desktop' | stipjeWhole };
    height: ${ 'desktop' | stipjeWhole };
    top:  calc(${ prop ('top')  >> String }% - ${ 'desktop' | stipjeHalf });
    left: calc(${ prop ('left') >> String }% - ${ 'desktop' | stipjeHalf });
  }
  z-index: ${ prop ('mouseOver') >> whenTrue (_ => '10') };
  border-radius: 10000px;
  background: ${'color' | prop};
  cursor: pointer;
  position: absolute;
`

class HotSpot extends PureComponent {
  constructor (props) {
    super (props)
    this.state = {
      mouseOver: false,
    }
  }
  render () {
    const { props, state, } = this
    const { onClick, idx, color, pos: [top, left], } = props

    const hover = x => this.setState ({
      mouseOver: x,
    })

    const { mouseOver, } = state

    return <HotSpotS
      onMouseOver={_ => true | hover}
      onMouseOut= {_ => false | hover}
      onTouchStart={_ => true | hover}
      onTouchEnd= {_ => false | hover}
      color={color}
      top={top}
      left={left}
      onClick={onClick}
      mouseOver={mouseOver}
    />
  }
}

const HotSpotsS = styled.div`
  z-index: 2;
  position: absolute;
  top: 0px;
  left: 0px;
  width: 100%;
  height: 70%;
`

const HotSpots = ({ click, }) => <HotSpotsS>
  {
    data | mapX (({
      color,
      pos,
      data: { language, profession, soundFile, locationGeneral, locationSpecific, },
    }, idx) => <HotSpot
      key={idx}
      idx={idx}
      pos={pos}
      color={color}
      onClick={_ => click (idx)}
    />
    )
  }
</HotSpotsS>

const footerWidth = 300

const FooterS = styled.div`
  font-size: 11px;
  color: black;
  margin: auto;
  width: ${_ => String (footerWidth) + 'px'};
  text-align: center;
  height: 21px;
`

const Footer = ({ children, }) => <FooterS>{children}</FooterS>

const drawerWidth = '100%'
const drawerTransitionLeft = ['100ms', '300ms']

const KaartS = styled.div`
  @media only screen and (max-width: 768px) {
    overflow-x: scroll;
    overflow-y: scroll;
  }
  img {
    height: 70%;
  }
  .x__drawer {
    width: ${ _ => drawerWidth };
    border: 1px solid black;
    height: 100%;
    position: absolute;
    z-index: 3;
    left: calc(-1 * ${ _ => drawerWidth });
    // --- height of map part minus title
    height: calc(70% - ${ 'titleHeight' | prop });
    top: ${ 'titleHeight' | prop };

    transition:   ${ _ => transitionStyle (['left'], [drawerTransitionLeft [0]]) };

    .x__bg {
      width: ${ _ => drawerWidth };
      height: 100%;
      z-index: 3;
      top: 50px;
      background: #000015;
      opacity: 0.5;
    }

    .x__inner {
      width: ${ _ => drawerWidth };
      left: inherit;
      height: 100%;
      position: absolute;
      z-index: 3;
      top: 50px;

      .x__slider-box {
        width: 100%;
        margin-top: 20%;
      }
      .x__slider-box > div:nth-child(2) {
        margin: auto;
        width: 60%;
        display: inline-block;
      }
      .x__slider-pitch {
      }
      .x__slider-zoom {
      }
      .x__icon {
        margin-right: 10px;
        display: inline-block;
        width: 32px;
        color: white;
      }
    }
  }
  .x__drawer.x--open {
    left: 0px;
    transition:   ${ _ => transitionStyle (['left'], [drawerTransitionLeft [1]]) };
  }
  .x__inner {
    font-size: 25px;
    overflow-x: hidden;
    display: inline-block;
    height: 100%;
    position: relative;
  }
  .x__controls {
    cursor: pointer;
    position: absolute;
    z-index: 4;
    top: ${ 'titleHeight' | prop };
    right: 20px;
    font-size: 30px;
    .x__menuicon {
      padding: 10px;
    }
  }
  text-align: center;
  height: calc(100% - 21px);

  position: relative;
  left: 0px;
  top: 0px;
  z-index: 1;
  margin-left: auto;
  margin-right: auto;
`

const Slider = (() => {
  const sliderTop = size => -9 / 20 * size + 11.5
  return ({ handleSize, min, max, value, defaultValue, onChange, }) => <RCSlider
    min={min}
    max={max}
    defaultValue={defaultValue}
    value={value}
    onChange={onChange}
    handleStyle={{
      width: handleSize + 'px',
      height: handleSize + 'px',
      top: (handleSize | sliderTop) + 'px',
    }}
  />
}) ()

const KaartImage = ({ children, img, titleHeight, }) => <KaartS
  titleHeight={titleHeight}
>
  <div className='x__inner'>
    <img src={img}/>
    {children}
  </div>
</KaartS>

const KaartMapboxS = styled.div`
  // --- necessary, but there is still a warning about missing css in the console xxx
  @import url('https://api.tiles.mapbox.com/mapbox-gl-js/v0.39.1/mapbox-gl.css');

  // --- before the width automatically b/c it was an image.
  @media only screen and (max-width: 768px) {
    width: 100vw;
  }
  @media only screen and (min-width: 768px) {
    width: 800px;
  }

  height: 70%;

  position: relative;
  canvas {
    left: 0px;
    top: 0px;
  }
  .mapboxgl-map {
    position: absolute;
    left: 0px;
    top: 0px;
    bottom: 0px;
    right: 0px;
  }
  .mapboxgl-control-container {
    display: none;
  }
`

const setStyle = s => el => (el.style | mergeM (s), el)

const getPopupText = (lan, pro) => [lan, pro] | join ('/')
const addMarker = (clicked, kaart, markerRef, { lat, lon, language, profession, }) => {
    const popup = new mapboxgl.Popup({ offset: 25, })
    .setText(getPopupText (language, profession))

  const m = new mapboxgl.Marker ({
    element: markerRef | setStyle ({ display: 'block', })
  })
  .setLngLat ([lon, lat])
  .setPopup (popup)
  .addTo (kaart)

  m.getElement ().addEventListener ('click', _ => {
    clicked (0)
  })
}

const addMapData = (clicked, kaart) => {
  data | mapX (({ color, data, latlon, }, idx) => {
    const [lat, lon] = latlon
    const { language, profession, soundFile, } = data
    const markerRef = markerRefs [idx]
    addMarker (_ => clicked (idx), kaart, markerRef, { lat, lon, language, profession, })
  })
}

const toggleState = curry ((ctxt, prop) => {
  const cur = ctxt.state [prop]
  const set = {}
  set [prop] = cur | not
  ctxt.setState (set)
})

const sliderHandleSize = 30

const transitionStyle = (props, times) => zip (props, times)
  | map (sprintfN ('%s %s'))
  | join (', ')

class KaartMapbox extends PureComponent {
  constructor (props) {
    super (props)
    this.state = {
      drawerOpen: false,

      // --- for keeping the controls updated.
      zoom: void 8,
      pitch: void 8,
    }

    this.kaart = void 8

    this.pitch = {
      initial: 40,
      min: 0,
      // --- seems to be the max possible.
      max: 60,
    }
    this.zoom = {
      initial: isMobile () ? 9 : 10,
      // --- min and max only apply to sliders; pinching/right-click is still unlimited.
      min: 7,
      max: 14,
    }
  }

  componentDidMount () {
    const { props, } = this
    const { hotspotClicked, } = props

    const kaart = new mapboxgl.Map ({
      container: this.mapContainer,
      style: 'mapbox://styles/allen-alleycat/cjjc04vxc6l6x2snztz93zjj0', // --- vintage
      center: [4.885, 52.370],
      pitch: this.pitch.initial,
      zoom: this.zoom.initial,
    })

    // --- can be used to load vector data, either straight from Mapbox studio or from json.
    false && kaart.on ('load', _ => kaart.addLayer ({
      id: 'geluiden',
      type: 'circle',
      source: {
        type: 'vector',
        url: 'mapbox://allen-alleycat.cjjc1vdlw0uvl2vkho87atbxs-0y4ua',
      },
      'source-layer': 'geluiden',
    }))

    kaart.on ('pitch', ({ target, }) => this.setState ({
      pitch: target.getPitch (),
    }))

    kaart.on ('zoom', ({ target, }) => this.setState ({
      zoom: target.getZoom (),
    }))

    setTimeout (_ => addMapData (hotspotClicked, kaart), 1000)

    this.kaart = kaart
  }

  barsTouchEnd () {
    this.toggleDrawer ()
  }

  barsClicked () {
    // --- dev tools.
    if (isMobile ()) return

    this.toggleDrawer ()
  }

  toggleDrawer () {
    const { drawerOpen, } = this.state
    'drawerOpen' | toggleState (this)
  }

  sliderPitchChange (val) {
    this.kaart.setPitch (val)
  }

  sliderZoomChange (val) {
    this.kaart.setZoom (val)
  }

  render () {
    const { props, state, } = this
    const { children, titleHeight, } = props
    const {
      drawerOpen,
      pitch: statePitch,
      zoom: stateZoom,
    } = state

    const { pitch, zoom, } = this

    const { min: sliderPitchMin, max: sliderPitchMax, initial: sliderPitchDefault, } = pitch
    const { min: sliderZoomMin, max: sliderZoomMax, initial: sliderZoomDefault, } = zoom

    const drawerClass = ['x__drawer'] | (drawerOpen | ifTrueV (
      concat (['x--open']),
      id,
    ))

    const sliderPitchValue = statePitch | defaultToV (sliderPitchDefault)
    const sliderZoomValue = stateZoom | defaultToV (sliderZoomDefault)

    return (
      <KaartS
        titleHeight={titleHeight}
      >
        <div className='x__inner'>
          <div
            className={drawerClass | join (' ')}
          >
            <div className='x__bg' />
            <div className='x__inner'>
              <div className='x__slider-box'>
                <div className='x__icon'>
                  <Arrow2 color='white' transform='scale(1.0, 1.0)'/>
                </div>
                <div className='x__slider-pitch'>
                  <Slider
                    min={sliderPitchMin}
                    max={sliderPitchMax}
                    // defaultValue={sliderPitchDefault}
                    value={sliderPitchValue}
                    handleSize={sliderHandleSize}
                    onChange={this | bindProp ('sliderPitchChange')}
                  />
                </div>
              </div>
              <div className='x__slider-box'>
                <div className='x__icon'>
                  <FontAwesome i='search' />
                </div>
                <div className='x__slider-zoom'>
                  <Slider
                    min={sliderZoomMin}
                    max={sliderZoomMax}
                    value={sliderZoomValue}
                    // defaultValue={sliderZoomDefault}
                    handleSize={sliderHandleSize}
                    onChange={this | bindProp ('sliderZoomChange')}/>
                </div>
              </div>
            </div>
          </div>
          <div
            className='x__controls'
            onTouchEnd={this | bindProp ('barsTouchEnd')}
            style={{ color: drawerOpen ? 'white' : 'black', }}
          >
            <div
              className='x__menuicon'
              onClick={this | bindProp ('barsClicked')}
            >
              <FontAwesome i='bars'/>
            </div>
          </div>
          <KaartMapboxS>
            <div ref={el => this.mapContainer = el} />
          </KaartMapboxS>
          { children }
        </div>
      </KaartS>
    )
  }
}

const infoBgColor = '#9fae489c'
const infoBgColor2 = '#f1f3f4'

const Arrow1 = ({ color = 'black', transform = '', }) => <svg x="0px" y="0px" viewBox="0 0 1000 1000">
  <g fill={color} transform={transform}><g><g><g><path d="M830.2,929.5c-192.9,0-518-56.8-551.9-433.4H10L435.6,70.5l425.6,425.6H581.8c-5.6,42.1-13.8,164,61.6,257.7c66.7,82.8,182.1,124.8,342.9,124.8l3.6,38.1C987.3,917,921.7,929.5,830.2,929.5z M102.7,457.6h211.5l1.2,18c20.4,321.5,258.6,405.2,471.7,414.6c-73.8-22.9-131.9-60.6-173.7-112.7c-103.7-129.1-68.1-297.7-66.5-304.8l3.3-15.1h218.3L435.6,124.8L102.7,457.6z"/></g></g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g><g></g></g>
</svg>

const Arrow2 = ({ color = 'black', transform = '', }) => <svg x="0px" y="0px" viewBox="0 0 1000 1000">
  <g><g><g><path d="path2" d="m 830.2,929.5 c -192.9,0 -526.47458,-5.95254 -560.37458,-382.55254 L 10,496.1 435.6,70.5 861.2,496.1 598.74915,538.47288 c -5.6,42.1 15.86102,125.86441 91.26102,219.56441 C 756.71017,840.83729 825.5,878.6 986.3,878.6 l 3.6,38.1 c -2.6,0.3 -68.2,12.8 -159.7,12.8 z M 183.20847,457.6 H 314.2 l 1.2,18 C 335.8,797.1 574,880.8 787.1,890.2 713.3,867.3 655.2,829.6 613.4,777.5 509.7,648.4 545.3,479.8 546.9,472.7 l 3.3,-15.1 H 713.41525 L 435.6,201.07119 Z" /></g></g><g/><g/><g/><g/><g/><g/><g/><g/><g/><g/><g/><g/><g/><g/><g/></g>
</svg>

const InfoS = styled.div`
  @media only screen and (max-width: 768px) {
    font-size: 15px;
    width: 100vw;
    height: 25%;
    position: fixed;
  }
  @media only screen and (min-width: 768px) {
    font-size: 24px;
    width: 100%;
    height: 30%;
    position: relative;
  }
  @media only screen and (min-width: 1024px) {
  }
  z-index: 2;

  margin: auto;
  white-space: nowrap;
  border-style: solid;
  border-color: ${ 'color' | prop };
  ${ prop ('show') >> whenFalse (_ => 'width: 0px;') }
  border-width: ${ prop ('show') >> ifTrue (_ => '2px',  _ => '0px')};
  opacity:      ${ prop ('show') >> ifTrue (_ => '1',    _ => '0')};
  transition:   ${ ({ show, }) => ['height', 'width', 'opacity']
    | map (prependTo ([show ? timeShow : timeHide]) >> sprintfN ('%s %s'))
    | join (', ')
  };
  overflow-x: hidden;
  overflow-y: hidden;
  .x__close {
    @media only screen and (max-width: 768px) {
      top: 2px;
      height: 20px;
      width: 20px;
      font-size: 15px;
    }
    @media only screen and (min-width: 768px) {
      top: 9px;
      height: 28px;
      width: 28px;
      font-size: 20px;
    }
    background: ${_ => infoBgColor2 };
    border-radius: 10000px;
    text-align: center;
    color: black;
    z-index: 4;
    position: absolute;
    right: 10px;
    cursor: pointer;
  }
  .x__text {
    div > div {
      display: inline-block;
      padding: 5px;
    }
    .x__headings {
      text-align: left;
      margin-left: 20%;
      @media only screen and (max-width: 768px) {
        height: 22px;
      }
      @media only screen and (min-width: 768px) {
        height: 38px;
      }

    }
    .x__heading {
      width: 28%;
    }
    @media only screen and (max-width: 768px) {
      height: calc(100% - 60px - 25px);
      padding-top: 0px;
      padding-bottom: 0px;
    }
    @media only screen and (min-width: 768px) {
      height: calc(100% - 60px - 50px);
      padding-top: 20px;
      padding-bottom: 20px;
    }
    position: relative;
    z-index: 3;
    overflow-y: hidden;
    text-align: center;
  }
  .x__player {
    text-align: center;
    position: relative;
    z-index: 3;
    margin-bottom: 10px;
  }
  .x__strip {
    @media only screen and (max-width: 768px) {
      height: 25px;
    }
    @media only screen and (min-width: 768px) {
      height: 50px;
    }
    width: 100%;
    background: ${ 'color' | prop };
    position: relative;
    top: 0px;
    left: 0px;
    z-index: 2;
  }
  .x__bg {
    position: absolute;
    z-index: 2;
    width: 100%;
    height: 100%;
    background: url(${ _ => infoBgImage });
    opacity: 0.3;
  }
  .x__stip {
    background: ${'color' | prop};
    border-radius: 10000px;
    width: 50px;
    height: 50px;
    position: absolute;
    left: 5%;
    top: 37%;
    color: yellow;
    z-index: 2;
  }
`

const Info = ({
  show, color, language, profession, soundFile,
  onClose, onSoundEnded,
}) => <InfoS
  show={show}
  color={color}
>
  <div className='x__bg'></div>
  <div className='x__strip'></div>
  { /* <div className='x__stip'></div> */}
  <div className='x__close' onClick={onClose}>✘</div>
  <div className='x__text'>
    <div className='x__headings'>
      <div className='x__heading'>Taal:</div>
      <div>{language}</div>
    </div>
    <div className='x__headings'>
      <div className='x__heading'>Beroep:</div>
      <div>{profession}</div>
    </div>
  </div>
  <div className='x__player'>
    { show | whenTrue (_ =>
      <ReactAudioPlayer
        src={soundFile}
        controls
        autoPlay
        onEnded={onSoundEnded}
      />
    )}
  </div>
</InfoS>

const TitleS = styled.div`
  @media only screen and (max-width: 768px) {
    font-size: 18px;
    position: fixed;
    padding-top: 9px;
  }
  @media only screen and (min-width: 768px) {
    font-size: 30px;
    position: absolute;
    padding-top: 15px;
  }
  top: 0px;
  height: ${ prop ('height') };
  width: 100%;
  max-width: 100vw;
  color: white;
  text-align: center;
  .x__contents {
    z-index: 10;
    position: relative;
    color: white;
  }
  .x__bg {
    position: absolute;
    top: 0px;
    height: ${ prop ('height') };
    width: 100%;
    background: ${ prop ('staticMap') >> ifTrue (_ => '#000011', _ => '#550011')};
    opacity: 0.7;
    z-index: 2;
  }
`

const Title = ({ children, height, staticMap, }) => <TitleS height={height} staticMap={staticMap}>
  <div className='x__bg' style={{ height, }}/>
  <div className='x__contents'>
    {children}
  </div>
</TitleS>

const isNotNegativeOne = eq (-1) >> not
const ifNotNegativeOne = isNotNegativeOne | ifPredicate
const whenNotNegativeOne = isNotNegativeOne | whenPredicate

const {
    data,
    mapImage,
    infoBgImage,
    animationTime: [timeShow, timeHide],
} = configGetsOrDie ({
    animationTime: 'animationTime',
    data: 'data',
    mapImage: ['images', 'map'],
    infoBgImage: ['images', 'infoBg'],
})

const info = (show,
  {
    onClose,
    onSoundEnded,
  } = {},
  {
    color,
    data: { language, profession, soundFile, } = {}
  } = {}) =>
  <Info
    show={show}
    onClose={onClose}
    onSoundEnded={onSoundEnded}
    color={color}
    language={language}
    profession={profession}
    soundFile={soundFile}
  />

const kaartImage = mapImage => (_1, _2, contents) => <KaartImage
  img={mapImage}
>
  { contents }
</KaartImage>

const kaartMapbox = (hotspotClicked, titleHeight, contents) => <KaartMapbox
  hotspotClicked={hotspotClicked}
  titleHeight={titleHeight}
>
  { contents }
</KaartMapbox>

const hotspotsReal = (clicked) => <HotSpots click={clicked} />

const Marker = ({ storeRef = noop, color='#3FB1CE', height='41px', width='27px', style, }) => <svg
  height={height}
  width={width}
  viewBox='0 0 27 41'
  style={style}
  ref={storeRef}
>

  <g fillRule='nonzero'>
    <g transform="translate(3.0, 29.0)" fill="#000000">
      <ellipse opacity="0.04" cx="10.5" cy="5.80029008" rx="10.5" ry="5.25002273"/>
      <ellipse opacity="0.04" cx="10.5" cy="5.80029008" rx="10.5" ry="5.25002273"/>
      <ellipse opacity="0.04" cx="10.5" cy="5.80029008" rx="9.5" ry="4.77275007"/>
      <ellipse opacity="0.04" cx="10.5" cy="5.80029008" rx="8.5" ry="4.29549936"/>
      <ellipse opacity="0.04" cx="10.5" cy="5.80029008" rx="7.5" ry="3.81822308"/>
      <ellipse opacity="0.04" cx="10.5" cy="5.80029008" rx="6.5" ry="3.34094679"/>
      <ellipse opacity="0.04" cx="10.5" cy="5.80029008" rx="5.5" ry="2.86367051"/>
      <ellipse opacity="0.04" cx="10.5" cy="5.80029008" rx="4.5" ry="2.38636864"/>
    </g>
    <g fill={color}>
      <path d="M27,13.5 C27,19.074644 20.250001,27.000002 14.75,34.500002 C14.016665,35.500004 12.983335,35.500004 12.25,34.500002 C6.7499993,27.000002 0,19.222562 0,13.5 C0,6.0441559 6.0441559,0 13.5,0 C20.955844,0 27,6.0441559 27,13.5 Z"/>
    </g>
    <g opacity="0.25" fill="#000000">
      <path d="M13.5,0 C6.0441559,0 0,6.0441559 0,13.5 C0,19.222562 6.7499993,27 12.25,34.5
        C13,35.522727 14.016664,35.500004 14.75,34.5 C20.250001,27 27,19.074644 27,13.5
        C27,6.0441559 20.955844,0 13.5,0 Z M13.5,1 C20.415404,1 26,6.584596 26,13.5 C26,15.898657
        24.495584,19.181431 22.220703,22.738281 C19.945823,26.295132 16.705119,30.142167 13.943359,33.908203
        C13.743445,34.180814 13.612715,34.322738 13.5,34.441406 C13.387285,34.322738 13.256555,34.180814
        13.056641,33.908203 C10.284481,30.127985 7.4148684,26.314159 5.015625,22.773438 C2.6163816,19.232715
        1,15.953538 1,13.5 C1,6.584596 6.584596,1 13.5,1 Z"/>
    </g>
    <g transform="translate(6.0, 7.0)" fill="#FFFFFF"/>
    <g transform="translate(8.0, 8.0)">
      <circle fill="#000000" opacity="0.25" cx="5.5" cy="5.5" r="5.4999962"/>
      <circle fill="#FFFFFF" cx="5.5" cy="5.5" r="5.4999962"/>
    </g>
  </g>
</svg>

const FontAwesomeS = styled.span`
  font-family: 'FontAwesome';
`

const FontAwesome = ({ i, }) => {
  const className = ['fas', 'fa-' + i] | join (' ')
  return <FontAwesomeS className={className} />
}

const titleHeight = isMobile () ? '45px' : '80px'

export class HomePage extends PureComponent {
  constructor (props) {
    super (props)
    this.state = {
      selectedIdx: -1,
    }
  }

  // --- must depend on a prop (ok).
  // hasData () {
  //   --- example for list (be sure to use immutable version)
  //   return this.props.assignedKeysCanonical | whenOk (isNotEmptyList)
  // }

  componentWillReceiveProps (nextProps) {
    if (localConfig.debug.render) whyYouRerender ('main', this.props, nextProps)
  }

  hotspotClicked (idx) {
    this.setState ({
      selectedIdx: idx,
    })
  }

  closeInfo () {
    this.setState ({
      selectedIdx: -1,
    })
  }

  soundEnded () {
    this.closeInfo ()
  }

  render () {
    const { state, props, } = this
    const { selectedIdx, } = state
    const { staticMap, } = props

    tell ('rendering main')
    logAndroidPerf ('(re)render HomePage')

    // --- note: be sure that the props which are function calls depend on props somewhere down the
    // line, or else they won't update.

    const kaart = staticMap ? kaartImage (mapImage) : kaartMapbox
    const hotspots = staticMap ? hotspotsReal : always (null)

    return (
      <Article>
        <Top>
          { data | mapX (({ color, }, idx) => <Marker
              key={idx}
              color={color}
              style={{ display: 'none', }}
              storeRef={ref => markerRefs [idx] = ref}
            />
          ) }

          { kaart (
            this | bindProp ('hotspotClicked'),
            titleHeight,
            <div>
              <Title height={titleHeight} staticMap={staticMap}>
                Geluiden van de Werkvloer
              </Title>

              { hotspots (this | bindProp ('hotspotClicked')) }

              { selectedIdx | ifNotNegativeOne (
                idx => info (
                  true,
                  {
                    onSoundEnded: this | bindProp ('soundEnded'),
                    onClose: this | bindProp ('closeInfo'),
                  },
                  data [idx]
                ),
                _ => info (false)
              ) }
            </div>
          ) }

        <Footer>
          Site by AlleyCat Amsterdam | <a target='_blank' href="http://alleycat.cc">alleycat.cc</a>
        </Footer>

        </Top>
      </Article>

    )
  }
}

HomePage.propTypes = {
}

const mapDispatchToProps = (dispatch) => ({
  // --- examples.
  // showAddKeyDispatch: showAddKey >> dispatch,
  // readerIDDispatch: path (['target', 'value']) >> changeReaderID >> dispatch,
})

const mapStateToProps = createStructuredSelector ({
})

export default HomePage
  | connect       (mapStateToProps, mapDispatchToProps)
  | injectSaga    ({ saga, key: 'HomePage', })
