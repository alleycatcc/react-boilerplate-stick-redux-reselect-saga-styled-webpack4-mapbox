defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

const config = {
  verbose: true,
}

import {
  pipe, compose, composeRight,
} from 'stick-js'

import { call, put, select, takeLatest, take, } from 'redux-saga/effects'
import request from 'utils/request'

// import { } from '../App/actions'

import {
  toJS, iwarn, whenNotEmptyObj, isTrue, tellIf,
  resolveP,
} from '../../utils/utils.js'

// import { } from '../../containers/App/constants'

// import { } from '../../domains/domain/selectors'

// import { } from '../../utils/utils.js'

const tell = tellIf (config.verbose)

export default function* rootSaga () {
}
