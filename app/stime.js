defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import ramda, {
  curry, mapAccum,
} from 'ramda'

import {
  pipe, compose, composeRight,
  tap, prop, head, rangeTo,
  ok, ifOk, ifTrue, ifFalse, ifYes, ifNo, ifPredicate, ifEmpty,
  condS, otherwise,
  sprintf1, sprintfN, appendM,
  factory, factoryProps,
  eq, lt,
  arg2, mergeM,
  defaultTo,
  lets, letS,
  guardV,
} from 'stick-js'

import {
  divMod,
  iwarn,
} from './utils/utils'

const findPredicate = curry ((f, xs) => {
  for (const i of xs) {
    const p = i | f
    if (p) return p
  }
  return void 8
})

const findWith = curry ((f, xs) => {
  for (const i of xs) {
    const [p, v] = i | f
    if (p) return v
  }
  return void 8
})

const ampm = hh => hh | condS ([
  0  | eq   | guardV ([12, 'pm']),
  12 | lt   | guardV ([hh, 'am']),
  otherwise | guardV ([hh - 12, 'pm']),
])

const proto = {
  initFromDateStr (str) {
    return this.initFromDate (new Date (str))
  },

  initFromDateMS (ms) {
    return this.initFromDate (new Date (ms))
  },

  initFromMinutes (minutes) {
    return this | mergeM ({ minutes, })
  },

  initFromDate (date) {
    const mm = date | letS ([
      (d) => d.getUTCHours (),
      (d) => d.getUTCMinutes (),
      (d, hh, mm) => hh * 60 + mm,
    ])
    return this | mergeM ({ minutes: mm })
  },

  getMinutes () {
    return this.minutes
  },

  getNearestMinutes (segmentLength, aMap) {
    return makeSnakeList (segmentLength)
      | findWith ((x) => {
        const mins = this.minutes + x
        return [aMap.has (mins), mins]
      })
      | defaultTo (_ => iwarn (
        this.minutes | sprintf1 ('No luck searching near %s in'), aMap
      ))
  },

  // --- get it every time, or else a long-lived object could end up skewed
  getTimezoneOffset () {
    return new Date ().getTimezoneOffset ()
  },

  print () {
    return lets (
      _ => this.minutes - this.getTimezoneOffset (),
      (mm) => mm % 1440,
      (_, mm) => mm | divMod (60),
      arg2 >> print24,
    )
  }
}

const makeSnakeList = (() => {
  const f = (acc, v) => [
    acc | appendM (-1 * v) | appendM (v),
    null,
  ]
  return (segmentLength) => 0
    | rangeTo (segmentLength)
    | mapAccum (f, [])
    | prop (0)
}) ()

// --- [hh, mm]
const print12 = letS ([
    head >> ampm,
    ([_3, mm], [hh, ampm]) => [hh, mm, ampm] | sprintfN ('%d:%02d %s'),
])

// --- [hh, mm]
const print24 = ([hh, mm]) => [hh, mm] | sprintfN ('%d:%02d')

const props = {
  // hours: 0,
  minutes: 0,
  // --- amsterdam
  timezoneOffset: -120,
}

export default proto | factory | factoryProps (props)
