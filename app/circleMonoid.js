defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import { curry, } from 'ramda'

import {
  pipe, compose, composeRight,
  not,
  otherwise, whenOk,
  guard, dot1, gte, lt,
  factory, factoryProps, factoryStatics,
  mergeM,
  raise, exception,
  die,
  guardV,
  condS, cond,
  assocM,
  againstAll,
  isInteger,
} from 'stick-js'

const both = (f, g) => againstAll ([f, g])
const isNonNegative = 0 | gte
const isNonNegativeInt = both (isInteger, isNonNegative)
const isNotNonNegativeInt = isNonNegativeInt >> not

// --- we're a bit loose with the definition of monoid: to combine an existing clock window and its
// associated value with a new value, it's enough to just provide the second value, not a whole new
// 'monoid' with its own window etc.
// --- we provide a 'zero' monoid for completeness (static function on the factory), though it
// doesn't really do anything useful.
// --- the upper value is non-inclusive

const checkInit = ({ lower, upper, value }) => cond (
  (_ => upper <= lower)
    | guardV ('upper must be greater than lower'),
  (_ => isNotNonNegativeInt (upper) || isNotNonNegativeInt (lower))
    | guardV ('upper and lower must be non-negative integers'),
    // (_ => value < lower || value >= upper)
    // | guardV ('value must be in the range [lower, upper)'),
  otherwise | guardV (null),
) | whenOk (exception)

const proto = {
  init ({ lower, upper, value, }) {
    checkInit ({ lower, upper, value}) | whenOk (raise)
    return this
      | mergeM ({ lower, upper, })
      | force (value)
      | assocM ('hasValue', true)
  },

  _force (sign, comp, value) {
    const l1 = this.length () + 1
    while (value | comp) value -= sign * l1
    return this.updateValue (value)
  },

  updateValue (value) {
    return this.clone () | assocM ('value', value)
  },

  add (value) {
    return this.force (this.value + value)
  },

  minus (value) {
    return this.force (this.value - value)
  },

  force (value) {
    const testupper = this.upper | gte
    const testlower = this.lower | lt
    return value | condS ([
      testupper  | guard (_ => this._force (+1, testupper, value)),
      testlower  | guard (_ => this._force (-1, testlower, value)),
      otherwise  | guard (_ => this.hasValue ? this.updateValue (value)
                                             : (this | assocM ('value', value))
      ),
    ])
  },

  distanceTo (value) {
    if (value >= this.upper || value < this.lower)
      die ('bad value', value)
    const tvalue = this.value
    return tvalue > value ? (this.upper - tvalue) + (value - this.lower)
                          : value - tvalue
  },

  length () {
    return this.upper - this.lower - 1
  },

  clone () {
    const { lower, upper, value, } = this
    return theFactory.create ().init ({ lower, upper, value, })
  },
}

const props = {
  hasValue: false,
  lower: void 8,
  upper: void 8,
  value: void 8,
}

const statics = {
  zero: _ => theFactory.create ({ lower: 0, upper: 1, value: 0, })
}

const theFactory = proto | factory | factoryStatics (statics) | factoryProps (props)

export const add = dot1 ('add')
export const minus = dot1 ('minus')
export const distanceTo = dot1 ('distanceTo')
export const plus  =  add
export const force = dot1 ('force')

export const circleMonoid = curry ((lower, upper, value) =>
    theFactory.create ().init ({ lower, upper, value, })
)

export default theFactory
