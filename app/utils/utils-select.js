defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ok,
} from 'stick-js'

import { createSelectorCreator, defaultMemoize, } from 'reselect'

export const createLengthEqualSelector = createSelectorCreator (
  defaultMemoize,
  (a, b) => ok (a) && ok (b) && a.size === b.size,
)

