defineBinaryOperator ('|',  (...args) => pipe         (...args))
defineBinaryOperator ('<<', (...args) => compose      (...args))
defineBinaryOperator ('>>', (...args) => composeRight (...args))

import {
  pipe, compose, composeRight,
  ok, mergeFrom, whenOk, dot, xMatch,
  whenPredicate, eq, repeat, mapPairs,
  ifTrue,
} from 'stick-js'

import {
  toJS, fromJS, getIn,
} from '../../utils/utils-immutable'

import {
  FETCH_FORECAST,
  FETCH_FORECAST_SUCCESS,
  FETCH_FORECAST_ERROR,
} from '../../containers/App/constants'

const initialState = fromJS ({
  loading: false,
})

export default (state = initialState, action) => {
  switch (action.type) {

    case FETCH_FORECAST:
    case FETCH_FORECAST_SUCCESS:
    case FETCH_FORECAST_ERROR:
      return state
        .set ('loading', false)

    default:
      return state
  }
}
